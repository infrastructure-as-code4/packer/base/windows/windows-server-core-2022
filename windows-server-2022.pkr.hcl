##################################################################################
# LOCALS
##################################################################################

locals {
  buildtime = formatdate("YYYY-MM-DD hh:mm ZZZ", timestamp())
}

##################################################################################
# SOURCE
##################################################################################

source "vsphere-iso" "windows-server-core-2022" {
  vcenter_server = var.vcenter_server
  username = var.vcenter_username
  password = var.vcenter_password
  insecure_connection = var.vcenter_insecure_connection
  host = var.vcenter_host
  datacenter = var.vcenter_datacenter
  datastore = var.vcenter_datastore
  folder = var.vcenter_folder
  vm_name = var.vm_name
  RAM = var.vm_mem_size
  CPUs = var.vm_cpu_sockets
  notes = "Build: ${local.buildtime}"
  tools_upgrade_policy = true
  remove_cdrom = true
  convert_to_template = true
  guest_os_type = var.vm_guest_os_type
  vm_version = var.vm_version
  video_ram = 12288
  firmware = var.vm_firmware
  cpu_cores = var.vm_cpu_cores
  CPU_hot_plug = false
  RAM_hot_plug = false
  cdrom_type = var.vm_cdrom_type
  disk_controller_type = var.vm_disk_controller_type
  storage {
    disk_size = var.vm_disk_size
    disk_controller_index = 0
    disk_thin_provisioned = true
  }
  network_adapters {
    network = var.vcenter_network
    network_card = var.vm_network_card
  }
#  iso_urls = var.iso_urls
  iso_paths = var.iso_paths
#  iso_checksum = var.iso_checksum
  floppy_files = var.floppy_files
  boot_order = "disk,cdrom"
  boot_wait = var.vm_boot_wait
  boot_command = var.vm_boot_command
  ip_wait_timeout = "60m"
  communicator = "winrm"
  winrm_username = var.winrm_username
  winrm_password = var.winrm_password
}


##################################################################################
# BUILD
##################################################################################

build {
  sources = [
    "source.vsphere-iso.windows-server-core-2022"
  ]

  provisioner "file" {
    source = "./answer_files/deploy.xml"
    destination = "C:\\Users\\Administrator\\Documents\\deploy.xml"
  }
  provisioner "file" {
    source = "./scripts"
    destination = "C:\\Users\\Administrator\\Documents"
  }

  provisioner "windows-shell" {
    inline = ["c:\\Windows\\System32\\Sysprep\\sysprep.exe /generalize /oobe /shutdown /quiet /unattend:C:\\Users\\Administrator\\Documents\\deploy.xml"]
  }
}